
import matplotlib.pyplot as plt
import numpy as np
from pylab import legend, loglog, show, title, xlabel, ylabel


# ---------------------------------------------------------------------------------------
#                           	 Error vs. N plots 
# ---------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------
#                        	 PLOTS FOR  u = x(1-x)y(1-y) 
# ---------------------------------------------------------------------------------------

# lambda = 1 
N = np.array([1089, 4225, 16641, 66049, 263169, 1050625])
el2_1 = np.array([4.8539e-11, 2.49316e-11, 2.61394e-10, 1.3169e-10  ,
				6.60989e-11, 3.31135e-11])
einf_1 = np.array([3.28995e-09, 3.27825e-09, 6.76808e-08, 6.7668e-08,
				6.76647e-08, 6.76636e-08])
				
plt.figure(1) 
plt.subplot(311)			
plt.loglog(N, el2_1, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_1, 'g', N, 0.9 * 1/N, 'g--')
title('SNES ex5 MMS $u = x(1-x)y(1-y)$') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 1$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'],  'upper right',
         fancybox=True, shadow=True) 


#lambda = 2 

el2_2 = np.array([9.8809e-12, 5.08033e-12, 2.57753e-12, 2.74502e-10 ,
				1.37782e-10, 6.9024e-11])
einf_2 = np.array([7.26412e-10, 7.25333e-10, 7.23808e-10, 1.526e-07,
				1.5259e-07, 1.52586e-07])

plt.subplot(312)
plt.loglog(N, el2_2, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_2, 'g', N, 0.9 * 1/N, 'g--') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 2$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right')
		
#lambda = 5 

el2_5 = np.array([2.1363e-11, 6.19761e-17, 7.13784e-12, 2.85993e-12 ,
				1.41219e-12, 1.36924e-18])
einf_5 = np.array([1.78262e-09, 1.23512e-14, 4.06079e-09, 2.28485e-09,
				1.89407e-09, 3.45557e-15])

plt.subplot(313)
plt.loglog(N, el2_5, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_5, 'g', N, 0.9 * 1/N, 'g--') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 5$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right')
          
plt.subplots_adjust(hspace=.4)
plt.show()


# ---------------------------------------------------------------------------------------
#                         PLOTS FOR  u = sin(pi*x)*sin(pi*y) 
# ---------------------------------------------------------------------------------------

# lambda = 1 
el2_12 = np.array([1.2437e-05, 1.60219e-06 , 2.03367e-07, 2.5442e-08  ,
				3.12599e-09, 3.58176e-10])
einf_12 = np.array([0.000846495, 0.00021154, 5.28788e-05, 1.31482e-05,
				3.23343e-06, 7.54757e-07])
				
plt.figure(2) 
plt.subplot(311)			
plt.loglog(N, el2_12, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_12, 'g', N, 0.9 * 1/N, 'g--')
title('SNES ex5 MMS $u = \sin(\pi x) \sin(\pi y)$') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 1$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right') 


#lambda = 2 

el2_22 = np.array([1.27387e-05 , 1.64097e-06, 2.07866e-07, 2.60227e-08 ,
				3.18397e-09, 3.57927e-10])
einf_2 = np.array([0.000872202, 0.000217945, 5.43638e-05, 1.35025e-05,
				3.28751e-06, 7.33791e-07])

plt.subplot(312)
plt.loglog(N, el2_22, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_2, 'g', N, 0.9 * 1/N, 'g--') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 2$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right')
		
#lambda = 5 

el2_52 = np.array([1.3124e-05, 1.69031e-06, 2.14513e-07, 2.6795e-08 ,
				3.27493e-09, 3.66404e-10])
einf_52 = np.array([ 0.000915985, 0.000228809, 5.71806e-05, 1.4145e-05,
				3.42148e-06, 7.40646e-07])

plt.subplot(313)
plt.loglog(N, el2_52, 'r', N, 0.9 * N ** -1.5, 'r--', N, einf_52, 'g', N, 0.9 * 1/N, 'g--') 
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$ for $\lambda = 5$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$', 
		'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right')
          
plt.subplots_adjust(hspace=.4)
plt.show()


# ---------------------------------------------------------------------------------------
#                           	 Work precision diagrams 
# ---------------------------------------------------------------------------------------

# ====================  COMPARISON OF SNES NEWTON LU VS. GRMES/GMG   ====================

# ---------------------------------------------------------------------------------------
#                              PLOTS FOR  u = x(1-x)y(1-y) 
# ---------------------------------------------------------------------------------------

plt.figure(3)

# lambda = 1

flops_11_lu = np.array([4.08354e+06 , 3.3465e+07, 2.27433e+08, 1.83915e+09 ,
				1.47975e+10])
el2_11_lu = np.array([4.85389e-11 , 2.49318e-11, 2.61394e-10, 1.31691e-10 ,
				6.6099e-11])
einf_11_lu = np.array([3.2901e-09 , 3.27831e-09, 6.76812e-08, 6.7668e-08 ,
				6.76647e-08])
				
flops_11_gmres = np.array([1.42983e+07 , 5.9166e+07, 2.01828e+08, 8.13031e+08 ,
				3.44967e+09])
el2_11_gmres = np.array([4.85391e-11 , 2.494e-11, 2.6156e-10, 1.31816e-10 ,
				6.6101e-11])
einf_11_gmres = np.array([3.29008e-09, 3.27923e-09, 6.77234e-08,  6.7729e-08 ,
				6.76719e-08])


				
plt.subplot(311)			
plt.loglog(flops_11_lu, el2_11_lu, 'r', flops_11_lu, einf_11_lu, 'r--', flops_11_gmres, el2_11_gmres, 'g', flops_11_gmres, einf_11_gmres, 'g--')
title('SNES Newton using LU & GMRES/GMG ex5 MMS $u = x(1-x)y(1-y)$') 
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 1$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))
		
# lambda = 2

flops_12_lu = np.array([2.72817e+06 , 2.23325e+07, 1.81999e+08, 1.10392e+09 ,
				8.88017e+09])
el2_12_lu = np.array([9.88353e-12 , 5.07974e-12 , 2.57747e-12 , 2.74502e-10 ,
				1.37782e-10])
einf_12_lu = np.array([7.26624e-10 , 7.24199e-10, 7.23595e-10, 1.52595e-07 ,
				1.52589e-07])
				
flops_12_gmres = np.array([9.56016e+06 , 3.9446e+07, 1.62314e+08, 6.5399e+08 ,
				2.04522e+09])
el2_12_gmres = np.array([9.88333e-12 , 5.08859e-12,2.58833e-12, 1.30838e-12 ,
				1.385e-10])
einf_12_gmres = np.array([7.26669e-10 , 7.25418e-10, 7.2708e-10, 7.28885e-10 ,
				1.53434e-07])


				
plt.subplot(312)			
plt.loglog(flops_11_lu, el2_11_lu, 'r', flops_11_lu, einf_11_lu, 'r--', flops_11_gmres, el2_11_gmres, 'g', flops_11_gmres, einf_11_gmres, 'g--')
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 2$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))
		
# lambda = 5

flops_15_lu = np.array([1.3728e+06 , 1.12001e+07, 9.11328e+07, 7.36296e+08 ,
				5.92152e+09])
el2_15_lu = np.array([2.13628e-11  , 1.0979e-11 , 5.57067e-12 , 2.80652e-12 ,
				1.40867e-12])
einf_15_lu = np.array([1.7828e-09 , 1.77558e-09, 1.77379e-09, 1.77334e-09 ,
				1.77323e-09])
				
flops_15_gmres = np.array([7.16889e+06 , 3.06798e+07, 1.26745e+08, 5.27241e+08 ,
				2.17516e+09])
el2_15_gmres = np.array([5.57363e-17 , 5.64676e-17,1.71457e-16, 4.8918e-17  ,
				7.30426e-18 ])
einf_15_gmres = np.array([6.33521e-15 , 1.38639e-14, 8.3912e-14, 4.33958e-14 ,
				1.72128e-14])


				
plt.subplot(313)			
plt.loglog(flops_15_lu, el2_15_lu, 'r', flops_15_lu, einf_15_lu, 'r--', flops_15_gmres, el2_15_gmres, 'g', flops_15_gmres, einf_15_gmres, 'g--')
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 5$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))



plt.subplots_adjust(hspace=.4)
plt.show()

# ---------------------------------------------------------------------------------------
#                              PLOTS FOR  u = sin(pi*x)*sin(pi*y)  
# ---------------------------------------------------------------------------------------

plt.figure(4)

# lambda = 1

flops_21_lu = np.array([3.40586e+06 , 2.78988e+07, 2.27433e+08, 1.47154e+09 ,
				1.18388e+10])
el2_21_lu = np.array([1.2437e-05  , 1.60219e-06 , 2.03367e-07, 2.5442e-08 ,
				3.12599e-09])
einf_21_lu = np.array([0.000846495 , 0.00021154, 5.28788e-05, 1.31482e-05 ,
				3.23343e-06])
				
flops_21_gmres = np.array([1.19331e+07 ,4.9328e+07, 2.01828e+08, 6.70086e+08 ,
				2.74733e+09])
el2_21_gmres = np.array([1.2437e-05 , 1.60219e-06, 2.03367e-07 , 2.54417e-08 ,
				3.12592e-09 ])
einf_21_gmres = np.array([0.000846495, 0.00021154, 5.28788e-05,  1.31481e-05 ,
				3.23337e-06])


				
plt.subplot(311)			
plt.loglog(flops_21_lu, el2_21_lu, 'r', flops_21_lu, einf_21_lu, 'r--', flops_21_gmres, el2_21_gmres, 'g', flops_21_gmres, einf_21_gmres, 'g--')
title('SNES Newton using LU & GMRES/GMG ex5 MMS $u = sin(pi*x)*sin(pi*y)$') 
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 1$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))
		
# lambda = 2

flops_22_lu = np.array([4.08354e+06 , 3.3465e+07, 2.27433e+08, 1.83915e+09 ,
				1.47975e+10])
el2_22_lu = np.array([1.27387e-05 , 1.64097e-06, 2.07866e-07, 2.60227e-08,
				3.18397e-09])
einf_22_lu = np.array([0.000872202 , 0.000217945, 5.43638e-05, 1.35025e-05 ,
				3.28751e-06])
				
				
flops_22_gmres = np.array([1.42983e+07 , 5.9166e+07, 2.01828e+08, 8.13031e+08,
				3.38519e+09])
el2_22_gmres = np.array([1.27387e-05 , 1.64097e-06, 2.07866e-07, 2.60225e-08 ,
				3.18391e-09])
einf_22_gmres = np.array([0.000872202, 0.000217945, 5.43637e-05, 1.35024e-05 ,
				3.28744e-06])


				
plt.subplot(312)			
plt.loglog(flops_22_lu, el2_22_lu, 'r', flops_22_lu, einf_22_lu, 'r--', flops_22_gmres, el2_22_gmres, 'g', flops_22_gmres, einf_22_gmres, 'g--')
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 2$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))


plt.subplots_adjust(hspace=.4)

# lambda = 5

flops_25_lu = np.array([4.76123e+06 , 3.90312e+07, 3.18299e+08, 2.20677e+09 ,
				1.77561e+10])
el2_25_lu = np.array([1.3124e-05 , 1.69031e-06, 2.14513e-07, 2.6795e-08,
				3.27493e-09 ])
einf_25_lu = np.array([0.000915985 , 0.000228809,  5.71806e-05, 1.4145e-05 ,
				3.42148e-06])
				
				
flops_25_gmres = np.array([1.66766e+07 , 6.91041e+07, 2.80816e+08, 9.88247e+08 ,
				4.28114e+09])
el2_25_gmres = np.array([1.3124e-05 , 1.69031e-06, 2.14513e-07, 2.67949e-08 ,
				3.27493e-09 ])
einf_25_gmres = np.array([0.000915985 , 0.000228809, 5.71806e-05, 1.41449e-05 ,
				3.42148e-06])


				
plt.subplot(313)			
plt.loglog(flops_25_lu, el2_25_lu, 'r', flops_25_lu, einf_25_lu, 'r--', flops_25_gmres, el2_25_gmres, 'g', flops_25_gmres, einf_25_gmres, 'g--')
xlabel('Floating Point Operations $N$')
ylabel('Solution Error $e$ for $\lambda = 2$')
legend(['$\ell 2$ LU', '$\ell \infty$ LU', 
		'$\ell 2$ GMRES', '$\ell \infty$ GMRES'], 'upper right', bbox_to_anchor=(1.1, 1))


plt.subplots_adjust(hspace=.4)
plt.show()


