static char help[] = "Implements QR decomposition of a matrix.\n\n";

/*
CAAM 519 - Homework 3 - Fernando Lejarza

Algorithm that computes the QR factorization of a square matrix A using the Graham-Schmidt 
method to create orthonormal basis of A. 
*/ 


#include <petscksp.h> 
#include <stdio.h> 


// Function that performs the QR routine and returns Q and R for a given A 
#undef __FUNCT__
#define __FUNCT__ "QRDec"
PetscErrorCode QRDec(Mat A, Mat *Q, Mat *R)
{
  PetscErrorCode ierr;
  PetscInt size;
  Vec currCol;
  Vec currBasis;
  Vec oldBasis;
  PetscReal norm;
  PetscScalar dotProd;
  PetscScalar *basisPoint;
  PetscFunctionBegin;
  
  // Getting size of A
  ierr = MatGetSize(A, &size, NULL); CHKERRQ(ierr);
  printf("%d \n \n",size) ; 
  PetscInt *rowInd;
  ierr = PetscMalloc(sizeof(PetscInt)*size, &rowInd); CHKERRQ(ierr);
  for(PetscInt i=0; i<size; ++i)
    rowInd[i] = i;

  // Initializing matrices and vectors needed to perform orthogonalization
  ierr = MatCreateSeqDense(PETSC_COMM_SELF, size, size, NULL, Q); CHKERRQ(ierr);
  ierr = MatCreateSeqDense(PETSC_COMM_SELF, size, size, NULL, R); CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF, size, &currCol); CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF, size, &currBasis); CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF, size, &oldBasis); CHKERRQ(ierr);

  // Initial loop over the columns 
  for(PetscInt col=0; col<size; ++col) {
    ierr = MatGetColumnVector(A, currCol, col); CHKERRQ(ierr);
    ierr = VecCopy(currCol, currBasis); CHKERRQ(ierr);
      
    //Loop over previously computed basis vectors
    for(PetscInt i=0; i<col; ++i) {
      //Gets previously computed basis vector
      ierr = MatGetColumnVector(*Q, oldBasis, i); CHKERRQ(ierr);
      //Computing projection of currCol on oldBasis 
      ierr = VecDot(currCol,oldBasis,&dotProd); CHKERRQ(ierr);
      ierr = MatSetValue(*R,i,col,dotProd,INSERT_VALUES); CHKERRQ(ierr);
      ierr = VecAXPY(currBasis, -dotProd, oldBasis); CHKERRQ(ierr);
        
    }
    
    //Normalizing orthogonal basis 
    ierr = VecNorm(currBasis,NORM_2,&norm); CHKERRQ(ierr);
    PetscScalar norm_recip;
    norm_recip = 1.0/norm;
    ierr = VecScale(currBasis,norm_recip); CHKERRQ(ierr);
    
    /* Creating entries of R by taking the dot product of the columns of A and the 
    orthogonal basis */
    ierr = VecDot(currCol,currBasis,&dotProd); CHKERRQ(ierr);
    ierr = MatSetValue(*R,col,col,dotProd,INSERT_VALUES); CHKERRQ(ierr);
    
    // Creating entries of Q by making the columns the orthonormal basis 
    ierr = VecGetArray(currBasis, &basisPoint); CHKERRQ(ierr);
    ierr = MatSetValues(*Q, size, rowInd, 1, &col, basisPoint, INSERT_VALUES); CHKERRQ(ierr);
    ierr = MatAssemblyBegin(*Q, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*Q, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = VecRestoreArray(currBasis, &basisPoint); CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


// Main function that calls the QR Routine 
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **args)
{
  
  Mat            A, Q, R, QR;
  PetscErrorCode ierr;
  PetscInt       n = 4, val; // Setting size of the matrix 4 by 4 
  PetscInt       i, j;
  PetscScalar    *array;
  PetscReal      matrixNorm;

  PetscInitialize(&argc,&args,(char*)0,help);
    
  // Initializing A 
  ierr = MatCreateSeqDense(PETSC_COMM_SELF, n, n, NULL, &A); CHKERRQ(ierr);
  ierr = PetscMalloc(sizeof(PetscScalar)*n*n,&array); CHKERRQ(ierr);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  
  // Creating entries of random values for a matrix A 
  for(i = 0; i < n; i++) { 
  	for(j = 0; j < n; j++) { 
  		
  		val = rand() % rand() % 8; 
  		ierr = MatSetValue(A,i,j,val,INSERT_VALUES); CHKERRQ(ierr) ;  
  	}
  } 
  
  // Calling QR routine and printing results 
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatView(A, PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  printf("=========================================== \n");  
  printf("                  OUTPUTS                   \n"); 
  printf("=========================================== \n");
  printf("                  Matrix A:                   "); 
  ierr = MatView(A,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  
  ierr = QRDec(A, &Q, &R); CHKERRQ(ierr);


    
  ierr = MatMatMult(Q,R,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QR); CHKERRQ(ierr);
  ierr = MatAXPY(A,-1,QR,SAME_NONZERO_PATTERN); CHKERRQ(ierr);
  ierr = MatNorm(A,NORM_FROBENIUS,&matrixNorm); CHKERRQ(ierr);

  printf("------------------------------------------- \n");  
  printf("                  Matrix Q:                   ");
  ierr = MatView(Q,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  printf("------------------------------------------- \n"); 
  printf("                  Matrix R:                   ");
  ierr = MatView(R,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  printf("------------------------------------------- \n");
  printf("             ||A - QR || =  %f \n",matrixNorm); // Should be zero for correct QR
  
  ierr = PetscFinalize(); 
  
  return 0; 

}
